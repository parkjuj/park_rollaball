﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using TMPro;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Runtime.InteropServices;

public class PlayerController : MonoBehaviour
{

	public float speed;
	public TextMeshProUGUI countText;
	public GameObject winTextObject;

	private float movementX;
	private float movementY;

	private Rigidbody rb;
	private int count;

	AudioSource audioSource;

	public float jumpPower; 
	bool onGround = false;

	void Start()
	{

		audioSource = GetComponent<AudioSource>();

		rb = GetComponent<Rigidbody>();

		count = 0;

		SetCountText();

		winTextObject.SetActive(false);
	}

	private void Update()
	{
		onGround = Physics.Raycast(transform.position, Vector3.down, .51f);//Sets what "OnGround" is, going a bit below the player as well to avoid complications.

		if (Keyboard.current.shiftKey.isPressed) //Player sprint is bound to shift key from here. 
		{
			speed = 40f; //Accelerates the player.
		}
		else
			speed = 10f;//Base speed in Unity.

	}


    void FixedUpdate()
		{

			Vector3 movement = new Vector3(movementX, 0.0f, movementY);

			rb.AddForce(movement * speed);
		}

	void OnJump()//Creates a function to coincide with the OnJump command in the Input section. 
    {
		Jump();
    }

	void Jump()
    {
		if (onGround)//Makes it so player can't continuously jump in the air.
			rb.AddForce(Vector3.up * jumpPower); //Rises the player based on jumpPower set in Unity.
    }

	void OnTriggerEnter(Collider other)
	{
	
		if (other.gameObject.CompareTag("PickUp"))
		{
			other.gameObject.SetActive(false);

			count = count + 1;

			SetCountText();
		}
	}

	void OnMove(InputValue value)
	{
		Vector2 v = value.Get<Vector2>();

		movementX = v.x;
		movementY = v.y;
	}


	void SetCountText()
	{
		countText.text = "Count: " + count.ToString();

		if (count >= 13)
		{
			winTextObject.SetActive(true);
		}
	}
}
